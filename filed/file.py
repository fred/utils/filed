"""High-level API for files from fileman service."""
import math
import warnings
from array import array
from datetime import datetime
from io import SEEK_END, SEEK_SET, BufferedIOBase, BytesIO, UnsupportedOperation
from itertools import repeat, starmap
from mmap import mmap
from typing import Optional, Union, cast, overload
from uuid import UUID

from .client import FileClient

# Set the defalt buffer to 3 MB.
# The default limit for gRPC message is 4 MB. It needs to contain the file data itself and the rest of message.
DEFAULT_BUFFER_SIZE = 3 * 1024 ** 2


class File(BufferedIOBase):
    """Provide complete Binary I/O for a file in a fileman service.

    Raises:
        UnsupportedOperation: If the required operation is not supported in current mode.
    """

    @overload
    def __init__(self, client: FileClient, *, uid: str):
        ...

    @overload
    def __init__(self, client: FileClient, *, uuid: UUID):
        ...

    @overload
    def __init__(self, client: FileClient, *, name: str, mimetype: Optional[str] = None):
        ...

    def __init__(self, client: FileClient, *, uid: Optional[str] = None, uuid: Optional[UUID] = None,
                 name: Optional[str] = None, mimetype: Optional[str] = None):
        """Create new file object.

        Arguments:
            client: The file client.
            uid: The file UID.
            uuid: The file UUID. Deprecated in favor of `uid`.
            name: The name of a created file.
            mimetype: A MIME type of a created file.
        """
        self.client = client
        self._uid = None
        if uid:
            self._uid = uid
        elif uuid:
            warnings.warn("Argument uuid is deprecated in favor of uid.", DeprecationWarning, stacklevel=2)
            self._uid = str(uuid)
        if self._write_mode:
            if name is None:
                raise TypeError("One of uid and name must be provided.")
            # Leave the metadata empty, but initialize the internal buffer.
            # TODO: Use TypedDict for annotations.
            self._stat = {'name': name, 'mimetype': mimetype}
            self._buff: Optional[BytesIO] = None
        else:
            if name is not None:
                raise TypeError("uid and name can't be both provided.")
            if mimetype is not None:
                raise TypeError("uid and mimetype can't be both provided.")
            # Fetch metadata on read mode and leave the internal buffer empty.
            assert self._uid is not None  # noqa S101
            self._stat = self.client.stat(self._uid)
            self._buff = None

    @property
    def _write_mode(self) -> bool:
        """Return whether the file is in a write mode."""
        return self._uid is None

    @property
    def uid(self) -> Optional[str]:
        """Return file's UID."""
        return self._uid

    @property
    def uuid(self) -> Optional[UUID]:
        """Return file's UUID."""
        warnings.warn("Attribute uuid is deprecated in favor of uid.", DeprecationWarning, stacklevel=2)
        if self._uid is None:
            return None
        else:
            return UUID(self._uid)

    @property
    def create_datetime(self) -> datetime:
        """Return file's create datetime."""
        try:
            return cast(datetime, self._stat['create_datetime'])
        except KeyError:
            raise UnsupportedOperation("create_datetime is not supported in write mode.") from None

    @property
    def size(self) -> int:
        """Return file's size."""
        try:
            return cast(int, self._stat['size'])
        except KeyError:
            raise UnsupportedOperation("size is not supported in write mode.") from None

    @property
    def mimetype(self) -> Optional[str]:
        """Return file's MIME type."""
        return cast(str, self._stat['mimetype'])

    @property
    def name(self) -> str:
        """Return file's name."""
        return cast(str, self._stat['name'])

    @property
    def _raw(self) -> BytesIO:
        """Return the internal buffer. Create it if needed."""
        if self._buff is None:
            if self._write_mode:
                # Return a one time buffer to mock the API.
                return BytesIO()
            else:
                # Fetch the file.
                assert self._uid is not None  # noqa: S101
                self._buff = BytesIO(b''.join(self.client.read(self._uid, DEFAULT_BUFFER_SIZE)))
        return self._buff

    # Binary I/O API - IOBase https://docs.python.org/3/library/io.html#io.IOBase
    @property
    def closed(self) -> bool:
        """Return `False`, this file is never closed."""
        return False

    def flush(self) -> None:
        """Flush write buffer, if applicable.

        This actually creates the file in fileman.
        This is a noop if in read mode or if there is nothing to be written.
        Also ends the write mode on the file.
        """
        if self._write_mode and self._buff:
            # Prepare chunks.
            # Duplicate buffer to prevent unwated modification of self._buff.
            buff = BytesIO(self._buff.getvalue())
            buff_len = buff.seek(0, SEEK_END)
            buff.seek(0)
            num_chunks = max(math.ceil(buff_len / DEFAULT_BUFFER_SIZE), 1)
            # Based on 'repeatfunc' recipe from itertools.
            chunks = starmap(buff.read, repeat((DEFAULT_BUFFER_SIZE, ), num_chunks))

            self._uid = self.client.create(self.name, chunks, mimetype=self.mimetype)
            # Fetch complete metadata
            self._stat = self.client.stat(self._uid)

    def readable(self) -> bool:
        """Return `True`, this stream can be read from."""
        return True

    def seek(self, offset: int, whence: int = SEEK_SET) -> int:
        """Change the stream position to the given byte offset."""
        return self._raw.seek(offset, whence)

    def seekable(self) -> bool:
        """Return `True`, this stream support random access."""
        return True

    def writable(self) -> bool:
        """Return `True` if this file is in a write mode."""
        return self._write_mode

    # Binary I/O API - BufferedIOBase https://docs.python.org/3/library/io.html#io.BufferedIOBase

    def read(self, size: Optional[int] = -1) -> bytes:
        """Read and return up to size bytes.

        If the argument is omitted, None, or negative, data is read and returned until EOF is reached.
        """
        return self._raw.read(size)

    def write(self, b: Union[bytes, bytearray, memoryview, array, mmap]) -> int:  # type: ignore[override] # _CData
        """Write data into an internal buffer."""
        if not self._write_mode:
            raise UnsupportedOperation("File is not in a write mode.")
        if not self._buff:
            self._buff = BytesIO()
        return self._buff.write(b)


class Storage:
    """Utility storage to retrieve files from single service."""

    def __init__(self, client: FileClient):
        self.client = client

    @overload
    def open(self, uid: str) -> File:
        ...

    @overload
    def open(self, uuid: UUID) -> File:
        ...

    def open(self, uid: Optional[Union[str, UUID]] = None, *, uuid: Optional[UUID] = None,  # type: ignore[misc]
             ) -> File:
        """Return a file with corresponding UID.

        Arguments:
            uid: A file UID.
            uuid: A file UUID. Deprecated in favor of `uid`.
        """
        if isinstance(uid, UUID):
            uid, uuid = None, uid
        if uuid:
            warnings.warn("Argument uuid is deprecated in favor of uid.", DeprecationWarning, stacklevel=2)
        return File(self.client, uid=uid or str(uuid))

    def create(self, name: str, *, mimetype: Optional[str] = None) -> File:
        """Create a new file."""
        return File(self.client, name=name, mimetype=mimetype)
