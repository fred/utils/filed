"""Filed - a client library for a Fred fileman."""
from .client import FileClient
from .file import File, Storage

__version__ = '0.4.0'
__all__ = ['File', 'FileClient', 'Storage']
