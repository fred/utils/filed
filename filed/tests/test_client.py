import types
from datetime import datetime
from typing import Any, Iterable, Iterator, Optional
from unittest import TestCase
from unittest.mock import ANY, call, sentinel
from uuid import UUID

from fred_api.fileman.service_fileman_grpc_pb2 import (
    CreateReply,
    CreateRequest,
    ReadReply,
    ReadRequest,
    StatReply,
    StatRequest,
)
from frgal.utils import TestClientMixin
from grpc import StatusCode
from grpc._channel import _RPCState, _SingleThreadedRendezvous as _Rendezvous
from pytz import UTC

from filed.client import FileClient


class TestFileClient(TestClientMixin, FileClient):
    """Testing version of a FileClient."""

    exhaust_iterables = False


class FileClientTest(TestCase):
    def setUp(self):
        self.client = TestFileClient(sentinel.netloc)

    def _test_stat(self, *args: Any, **kwargs: Any) -> None:
        uid = str(UUID(int=42))
        name = 'example.txt'
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        mimetype = 'text/plain'
        size = 1024
        reply = StatReply()
        reply.data.create_datetime.FromDatetime(create_datetime)
        reply.data.size = size
        reply.data.mimetype = mimetype
        reply.data.name = name
        self.client.mock.return_value = reply

        result = self.client.stat(*args, **kwargs)

        self.assertEqual(result, {'create_datetime': create_datetime, 'size': size, 'mimetype': mimetype, 'name': name})
        request = StatRequest()
        request.uid.value = uid
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/stat', timeout=None)])

    def test_stat_uid_arg(self):
        self._test_stat(str(UUID(int=42)))

    def test_stat_uid_kwarg(self):
        self._test_stat(uid=str(UUID(int=42)))

    def test_stat_uuid_arg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_stat(UUID(int=42))

    def test_stat_uuid_kwarg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_stat(uuid=UUID(int=42))

    def test_stat_does_not_exist(self):
        uid = str(UUID(int=42))
        error = _Rendezvous(_RPCState((), '', '', StatusCode.NOT_FOUND, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(FileNotFoundError, 'File {} not found.'.format(uid)):
            self.client.stat(uid)

        request = StatRequest()
        request.uid.value = uid
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/stat', timeout=None)])

    def test_stat_failed(self):
        uid = str(UUID(int=42))
        error = _Rendezvous(_RPCState((), '', '', StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(ConnectionAbortedError, 'Connection failed'):
            self.client.stat(uid)

        request = StatRequest()
        request.uid.value = uid
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/stat', timeout=None)])

    def _test_read(self, *args: Any, request_size: int = 0, **kwargs: Any) -> None:
        uid = str(UUID(int=42))
        content = b'Rimmer is a smeg head!'
        reply = ReadReply()
        reply.data.data = content
        self.client.mock.return_value = [reply]

        result = self.client.read(*args, **kwargs)

        self.assertIsInstance(result, types.GeneratorType)
        self.assertEqual(tuple(result), (content, ))
        request = ReadRequest()
        request.uid.value = uid
        request.size = request_size
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/read', timeout=None)])

    def test_read_uid_arg(self):
        self._test_read(str(UUID(int=42)))

    def test_read_uid_arg_size_arg(self):
        self._test_read(str(UUID(int=42)), 100, request_size=100)

    def test_read_uid_arg_size_kwarg(self):
        self._test_read(str(UUID(int=42)), size=100, request_size=100)

    def test_read_uid_kwarg(self):
        self._test_read(uid=str(UUID(int=42)))

    def test_read_uid_kwarg_size_kwarg(self):
        self._test_read(uid=str(UUID(int=42)), size=100, request_size=100)

    def test_read_uuid_arg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_read(UUID(int=42))

    def test_read_uuid_arg_size_arg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_read(UUID(int=42), 100, request_size=100)

    def test_read_uuid_arg_size_kwarg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_read(UUID(int=42), size=100, request_size=100)

    def test_read_uuid_kwarg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_read(uuid=UUID(int=42))

    def test_read_uuid_kwarg_size_kwargs(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_read(uuid=UUID(int=42), size=100, request_size=100)

    def test_read_chunked(self):
        uid = str(UUID(int=42))
        content = b'Rimmer is a smeg head!'
        reply1 = ReadReply()
        reply1.data.data = content[:12]
        reply2 = ReadReply()
        reply2.data.data = content[12:]
        self.client.mock.return_value = [reply1, reply2]

        result = self.client.read(uid, size=12)

        self.assertIsInstance(result, types.GeneratorType)
        self.assertEqual(tuple(result), (content[:12], content[12:]))

        # Finish and check request
        request = ReadRequest()
        request.uid.value = uid
        request.size = 12
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/read', timeout=None)])

    def test_read_does_not_exist(self):
        uid = str(UUID(int=42))
        error = _Rendezvous(_RPCState((), '', '', StatusCode.NOT_FOUND, ""), None, None, None)

        class _ErrorStream:
            def __iter__(self):
                raise error

        self.client.mock.return_value = _ErrorStream()

        with self.assertRaisesRegex(FileNotFoundError, 'File {} not found.'.format(uid)):
            next(self.client.read(uid))

        # Finish and check request
        request = ReadRequest()
        request.uid.value = uid
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/read', timeout=None)])

    def test_read_failed(self):
        uid = str(UUID(int=42))
        error = _Rendezvous(_RPCState((), '', '', StatusCode.UNKNOWN, ""), None, None, None)

        class _ErrorStream:
            def __iter__(self):
                raise error

        self.client.mock.return_value = _ErrorStream()

        with self.assertRaisesRegex(ConnectionAbortedError, 'Connection failed'):
            next(self.client.read(uid))

        # Finish and check request
        request = ReadRequest()
        request.uid.value = uid
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/read', timeout=None)])

    def _test_create(self, name: str, data: Iterable[bytes], *, mimetype: Optional[str] = None,
                     requests: Iterable[CreateRequest]) -> None:
        uid = str(UUID(int=42))
        reply = CreateReply()
        reply.data.uid.value = uid
        self.client.mock.return_value = reply

        result = self.client.create(name, data, mimetype=mimetype)

        self.assertEqual(result, uid)
        self.assertEqual(self.client.mock.mock_calls,
                         [call(ANY, method='/Fred.Fileman.Api.File/create', timeout=None)])
        # Check the iterator.
        req_iterator = self.client.mock.mock_calls[0][1][0]
        self.assertIsInstance(req_iterator, Iterator)
        self.assertEqual(tuple(req_iterator), tuple(requests))

    def test_create(self):
        name = 'example.txt'
        content = b'Rimmer is a smeg head!'

        request = CreateRequest()
        request.name = name
        request.data = content

        self._test_create(name, [content], requests=[request])

    def test_create_mimetype(self):
        name = 'example.txt'
        content = b'Rimmer is a smeg head!'
        mimetype = 'text/plain'

        request = CreateRequest()
        request.name = name
        request.data = content
        request.mimetype = mimetype

        self._test_create(name, [content], mimetype=mimetype, requests=[request])

    def test_create_chunks(self):
        # Test data are passed in chunks.
        name = 'example.txt'
        content = b'Rimmer is a smeg head!'
        mimetype = 'text/plain'

        request_1 = CreateRequest()
        request_1.name = name
        request_1.data = content[:12]
        request_1.mimetype = mimetype
        request_2 = CreateRequest()
        request_2.data = content[12:]

        self._test_create(name, (content[:12], content[12:]), mimetype=mimetype, requests=[request_1, request_2])

    def test_create_empty(self):
        # Test create file with no content.
        name = 'example.txt'

        # Use `_test_create` here, since the `FileClient.create` calls the gRPC `create` with an empty iterator.
        # In such case, the gRPC is not called, and no file is actually created.
        # Whole operation is effectively a noop.
        self._test_create(name, [], requests=[])

    def test_create_failed(self):
        error = _Rendezvous(_RPCState((), '', '', StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(ConnectionAbortedError, 'Connection failed'):
            self.client.create('example.txt', [b'Rimmer is a smeg head!'])
