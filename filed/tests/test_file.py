from datetime import datetime
from io import BytesIO, UnsupportedOperation
from typing import Any, Optional
from unittest import TestCase
from unittest.mock import ANY, call, patch, sentinel
from uuid import UUID

from fred_api.fileman.service_fileman_grpc_pb2 import (
    CreateReply,
    CreateRequest,
    ReadReply,
    ReadRequest,
    StatReply,
    StatRequest,
)
from grpc import StatusCode
from grpc._channel import _RPCState, _SingleThreadedRendezvous as _Rendezvous
from pytz import UTC

from filed.file import DEFAULT_BUFFER_SIZE, File, Storage

from .test_client import TestFileClient

A_UUID = UUID(int=42)
A_UID = str(UUID(int=42))
CONTENT = b"""Arnold Rimmer, Technician, 2nd Class.
Captain's remarks:
There's a saying amongst the officers:
"If a job's worth doing, it's worth doing well. If it's not worth doing, give it to Rimmer."
He aches for responsibility but constantly fails the engineering exam.
Astoundingly zealous.
Possibly mad.
Probably has more teeth than brain cells.
Promotion prospects: comical.
"""
CREATE_DATETIME = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
SIZE = len(CONTENT)
MIMETYPE = 'text/plain'
NAME = 'rimmer.dat'


class FileTest(TestCase):
    def setUp(self):
        self.client = TestFileClient(sentinel.netloc)

        stat_reply = StatReply()
        stat_reply.data.create_datetime.FromDatetime(CREATE_DATETIME)
        stat_reply.data.size = SIZE
        stat_reply.data.mimetype = MIMETYPE
        stat_reply.data.name = NAME
        read_reply = ReadReply()
        read_reply.data.data = CONTENT
        create_reply = CreateReply()
        create_reply.data.uid.value = A_UID

        def reply(*args, **kwargs):
            """Return a reply based on method called."""
            method = kwargs['method'].rpartition('/')[2]
            if method == 'stat':
                return stat_reply
            elif method == 'read':
                return (read_reply, )
            elif method == 'create':
                return create_reply
            raise RuntimeError('Unknown method')  # pragma: no cover
        self.client.mock.side_effect = reply

    def assertNoCalls(self):
        self.assertEqual(self.client.mock.mock_calls, [])

    def assertStatCalled(self):
        request = StatRequest()
        request.uid.value = A_UID
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/stat', timeout=None)])

    def assertReadCalled(self):
        read_request = ReadRequest()
        read_request.uid.value = A_UID
        read_request.size = DEFAULT_BUFFER_SIZE
        self.assertEqual(self.client.mock.mock_calls,
                         [call(read_request, method='/Fred.Fileman.Api.File/read', timeout=None)])

    def assertCreateCalled(self, content: bytes = CONTENT) -> None:
        create_request = CreateRequest()
        create_request.name = NAME
        create_request.data = content
        stat_request = StatRequest()
        stat_request.uid.value = A_UID

        calls = [call(ANY, method='/Fred.Fileman.Api.File/create', timeout=None),
                 call(stat_request, method='/Fred.Fileman.Api.File/stat', timeout=None)]
        self.assertEqual(self.client.mock.mock_calls, calls)
        # Check the iterator.
        req_iterator = self.client.mock.mock_calls[0][1][0]
        self.assertEqual(tuple(req_iterator), (create_request, ))

    # READ MODE ########################################################################################################

    def get_r_file(self) -> File:
        """Return a file in read mode and reset mock."""
        file = File(self.client, uid=A_UID)
        # Reset mock to ditch stat call from the file initialization.
        # This helps to monitor only calls from the test itself.
        self.client.mock.reset_mock()
        return file

    def test_r_init(self):
        file = File(self.client, uid=A_UID)

        self.assertEqual(file.uid, A_UID)
        self.assertStatCalled()

    def test_r_init_uuid(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            file = File(self.client, uuid=A_UUID)

        self.assertEqual(file.uid, A_UID)
        self.assertStatCalled()

    def test_r_init_not_found(self):
        error = _Rendezvous(_RPCState((), '', '', StatusCode.NOT_FOUND, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(FileNotFoundError, 'File {} not found.'.format(A_UID)):
            File(self.client, uid=A_UID)

        self.assertStatCalled()

    def test_r_init_failed(self):
        error = _Rendezvous(_RPCState((), '', '', StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(ConnectionAbortedError, 'Connection failed'):
            File(self.client, uid=A_UID)

        self.assertStatCalled()

    def test_r_context(self):
        with File(self.client, uid=A_UID) as file:
            self.assertEqual(file.uid, A_UID)

        self.assertStatCalled()

    def _test_r_metadata(self, name: str, value: Any) -> None:
        file = self.get_r_file()

        self.assertEqual(getattr(file, name), value)

        self.assertNoCalls()

    def test_r_uid(self):
        self._test_r_metadata('uid', A_UID)

    def test_r_uuid(self):
        with self.assertWarnsRegex(DeprecationWarning, "Attribute uuid is deprecated in favor of uid."):
            self._test_r_metadata('uuid', A_UUID)

    def test_r_create_datetime(self):
        self._test_r_metadata('create_datetime', CREATE_DATETIME)

    def test_r_size(self):
        self._test_r_metadata('size', SIZE)

    def test_r_mimetype(self):
        self._test_r_metadata('mimetype', MIMETYPE)

    def test_r_name(self):
        self._test_r_metadata('name', NAME)

    # Binary I/O API - IOBase https://docs.python.org/3/library/io.html#io.IOBase
    def test_r_close(self):
        # Test close is a noop in read mode.
        file = self.get_r_file()

        file.close()

        self.assertFalse(file.closed)
        self.assertNoCalls()

    def test_r_closed(self):
        # File is never closed.
        file = self.get_r_file()

        self.assertFalse(file.closed)

        self.assertNoCalls()

    def test_r_fileno(self):
        file = self.get_r_file()

        self.assertRaises(UnsupportedOperation, file.fileno)

        self.assertNoCalls()

    def test_r_flush(self):
        # Test flush is a noop in read mode.
        file = self.get_r_file()

        file.flush()

        self.assertNoCalls()

    def test_r_isatty(self):
        file = self.get_r_file()

        self.assertFalse(file.isatty())

        self.assertNoCalls()

    def test_r_readable(self):
        file = self.get_r_file()

        self.assertTrue(file.readable())

        self.assertNoCalls()

    def test_r_readline(self):
        file = self.get_r_file()

        self.assertEqual(file.readline(), b'Arnold Rimmer, Technician, 2nd Class.\n')

        self.assertReadCalled()

    def test_r_readline_size(self):
        file = self.get_r_file()

        self.assertEqual(file.readline(10), b'Arnold Rim')

        self.assertReadCalled()

    def test_r_readlines(self):
        file = self.get_r_file()

        self.assertEqual(file.readlines(), CONTENT.splitlines(keepends=True))

        self.assertReadCalled()

    def test_r_readlines_hint(self):
        file = self.get_r_file()

        self.assertEqual(file.readlines(40), CONTENT.splitlines(keepends=True)[:2])

        self.assertReadCalled()

    def test_r_seek(self):
        file = self.get_r_file()

        file.seek(10)

        self.assertEqual(file.tell(), 10)
        self.assertReadCalled()

    def test_r_seekable(self):
        file = self.get_r_file()

        self.assertTrue(file.seekable())

        self.assertNoCalls()

    def test_r_tell(self):
        file = self.get_r_file()

        self.assertEqual(file.tell(), 0)

        self.assertReadCalled()

    def test_r_truncate(self):
        file = self.get_r_file()

        self.assertRaises(UnsupportedOperation, file.truncate)

        self.assertNoCalls()

    def test_r_writable(self):
        # Test file is not writable in read mode.
        file = self.get_r_file()

        self.assertFalse(file.writable())

        self.assertNoCalls()

    def test_r_writelines(self):
        # Test writelines fails in read mode.
        file = self.get_r_file()

        self.assertRaises(UnsupportedOperation, file.writelines, CONTENT.splitlines())

        self.assertNoCalls()

    def test_r_del(self):
        file = self.get_r_file()

        del file

        self.assertNoCalls()

    def test_r_iter(self):
        file = self.get_r_file()

        iterator = iter(file)

        self.assertEqual(tuple(iterator), tuple(CONTENT.splitlines(keepends=True)))
        self.assertReadCalled()

    def test_r_next(self):
        file = self.get_r_file()

        self.assertEqual(next(file), b'Arnold Rimmer, Technician, 2nd Class.\n')

        self.assertReadCalled()

    # Binary I/O API - BufferedIOBase https://docs.python.org/3/library/io.html#io.BufferedIOBase
    def test_r_detach(self):
        file = self.get_r_file()

        self.assertRaises(UnsupportedOperation, file.detach)

        self.assertNoCalls()

    def test_r_read(self):
        file = self.get_r_file()

        self.assertEqual(file.read(), CONTENT)

        self.assertReadCalled()

    def test_r_read_size(self):
        file = self.get_r_file()

        self.assertEqual(file.read(size=10), b'Arnold Rim')

        self.assertReadCalled()

    def test_r_read_size_none(self):
        file = self.get_r_file()

        self.assertEqual(file.read(size=None), CONTENT)

        self.assertReadCalled()

    def test_r_read1(self):
        file = self.get_r_file()

        self.assertRaises(UnsupportedOperation, file.read1)

        self.assertNoCalls()

    def test_r_readinto(self):
        file = self.get_r_file()

        array = bytearray(10)
        self.assertEqual(file.readinto(array), 10)

        self.assertEqual(array, CONTENT[:10])
        self.assertReadCalled()

    def test_r_readinto1(self):
        file = self.get_r_file()

        self.assertRaises(UnsupportedOperation, file.readinto1, bytearray(10))

        self.assertNoCalls()

    def test_r_write(self):
        # Test write fails in read mode.
        file = self.get_r_file()

        self.assertRaises(UnsupportedOperation, file.write, BytesIO())

        self.assertNoCalls()

    def test_r_read_failed(self):
        # Return a valid reply for stat, but fail for read.
        reply = StatReply()
        error = _Rendezvous(_RPCState((), '', '', StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = [reply, error]

        file = self.get_r_file()

        with self.assertRaisesRegex(ConnectionAbortedError, 'Connection failed'):
            file.read()

        self.assertReadCalled()

    # WRITE MODE #######################################################################################################

    def get_w_file(self, content: Optional[bytes] = CONTENT) -> File:
        """Return a file in write mode with initial content."""
        file = File(self.client, name=NAME)
        if content is not None:
            file.write(content)
            file.seek(0)
        return file

    def test_w_init(self):
        # Test file in write mode.
        file = File(self.client, name=NAME)

        self.assertEqual(file.name, NAME)
        self.assertNoCalls()

    def test_w_init_mimetype(self):
        # Test file in write mode - define mimetype.
        file = File(self.client, name=NAME, mimetype=MIMETYPE)

        self.assertEqual(file.name, NAME)
        self.assertEqual(file.mimetype, MIMETYPE)
        self.assertNoCalls()

    def test_w_context(self):
        with File(self.client, name=NAME) as file:
            file.write(CONTENT)

        self.assertFalse(file.writable())
        self.assertCreateCalled()

    def test_w_context_empty(self):
        with File(self.client, name=NAME) as file:
            self.assertEqual(file.name, NAME)

        self.assertTrue(file.writable())
        self.assertNoCalls()

    def test_w_uid(self):
        file = File(self.client, name=NAME)

        self.assertIsNone(file.uid)

        self.assertNoCalls()

    def test_w_uuid(self):
        file = File(self.client, name=NAME)

        with self.assertWarnsRegex(DeprecationWarning, "Attribute uuid is deprecated in favor of uid."):
            self.assertIsNone(file.uuid)

        self.assertNoCalls()

    def _test_w_metadata_flushed(self, name: str, value: Any) -> None:
        # Test metadata are returned after file is flushed.
        file = self.get_w_file()
        file.flush()

        self.assertEqual(getattr(file, name), value)

        self.assertCreateCalled()

    def test_w_create_datetime(self):
        file = File(self.client, name=NAME)

        with self.assertRaises(UnsupportedOperation):
            file.create_datetime

        self.assertNoCalls()

    def test_w_create_datetime_flushed(self):
        self._test_w_metadata_flushed('create_datetime', CREATE_DATETIME)

    def test_w_size(self):
        file = File(self.client, name=NAME)

        with self.assertRaises(UnsupportedOperation):
            file.size

        self.assertNoCalls()

    def test_w_size_flushed(self):
        self._test_w_metadata_flushed('size', SIZE)

    def test_w_mimetype(self):
        file = File(self.client, name=NAME, mimetype=MIMETYPE)

        self.assertEqual(file.mimetype, MIMETYPE)

        self.assertNoCalls()

    def test_w_mimetype_none(self):
        file = File(self.client, name=NAME)

        self.assertIsNone(file.mimetype)

        self.assertNoCalls()

    def test_w_mimetype_flushed(self):
        self._test_w_metadata_flushed('mimetype', MIMETYPE)

    def test_w_name(self):
        file = File(self.client, name=NAME)

        self.assertEqual(file.name, NAME)

        self.assertNoCalls()

    def test_w_name_flushed(self):
        self._test_w_metadata_flushed('name', NAME)

    # Binary I/O API - IOBase https://docs.python.org/3/library/io.html#io.IOBase
    def test_w_close(self):
        # Test close file after write actually saves the data.
        file = self.get_w_file()

        file.close()

        # Check file is no longer writable.
        self.assertFalse(file.writable())
        self.assertFalse(file.closed)
        self.assertCreateCalled()

    def test_w_close_empty(self):
        # Test close file when nothing is written is a noop.
        file = self.get_w_file(None)

        file.close()

        # Check file is still writable.
        self.assertTrue(file.writable())
        self.assertFalse(file.closed)
        self.assertNoCalls()

    def test_w_closed_open(self):
        # Test closed returns True if not closed yet.
        file = self.get_w_file()

        self.assertFalse(file.closed)

        self.assertNoCalls()

    def test_w_closed_closed(self):
        # Test closed returns True even after close.
        file = self.get_w_file(None)
        file.close()

        self.assertFalse(file.closed)

        self.assertNoCalls()

    def test_w_fileno(self):
        file = self.get_w_file()

        self.assertRaises(UnsupportedOperation, file.fileno)

        self.assertNoCalls()

    def test_w_flush(self):
        # Test flush writes the content and ends the write mode.
        file = self.get_w_file()

        file.flush()

        # Check file is no longer writable.
        self.assertFalse(file.writable())
        self.assertCreateCalled()

    def test_w_flush_empty_content(self):
        # Test flush works for empty content the same.
        file = self.get_w_file(b'')

        file.flush()

        # Check file is no longer writable.
        self.assertFalse(file.writable())
        self.assertCreateCalled(content=b'')

    def test_w_flush_empty(self):
        # Test flush doesn't do anything, if no data were written.
        file = self.get_w_file(None)

        file.flush()

        # Check file is still writable.
        self.assertTrue(file.writable())
        self.assertNoCalls()

    def test_w_flush_chunks(self):
        # Test flush actually sends long data in chunks.
        file = self.get_w_file()

        with patch("filed.file.DEFAULT_BUFFER_SIZE", 100):
            file.flush()

        # Check calls
        stat_request = StatRequest()
        stat_request.uid.value = A_UID
        calls = [call(ANY, method='/Fred.Fileman.Api.File/create', timeout=None),
                 call(stat_request, method='/Fred.Fileman.Api.File/stat', timeout=None)]
        self.assertEqual(self.client.mock.mock_calls, calls)
        # Check the 'create' stream iterator.
        req_iterator = self.client.mock.mock_calls[0][1][0]
        create_requests = tuple(
            CreateRequest(data=c) for c in (CONTENT[:100], CONTENT[100:200], CONTENT[200:300], CONTENT[300:]))
        create_requests[0].name = NAME
        self.assertEqual(tuple(req_iterator), create_requests)

    def test_w_flush_failed(self):
        # Test flush fails on writing the content.
        error = _Rendezvous(_RPCState((), '', '', StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        file = self.get_w_file()

        with self.assertRaisesRegex(ConnectionAbortedError, 'Connection failed'):
            file.flush()

        # Check file is still writable.
        self.assertTrue(file.writable())

    def test_w_isatty(self):
        file = self.get_w_file()

        self.assertFalse(file.isatty())

        self.assertNoCalls()

    def test_w_readable(self):
        file = self.get_w_file()

        self.assertTrue(file.readable())

    def test_w_readline(self):
        file = self.get_w_file()

        self.assertEqual(file.readline(), b'Arnold Rimmer, Technician, 2nd Class.\n')

        self.assertNoCalls()

    def test_w_readline_empty(self):
        file = self.get_w_file(None)

        self.assertEqual(file.readline(), b'')

        self.assertNoCalls()

    def test_w_readline_size(self):
        file = self.get_w_file()

        self.assertEqual(file.readline(10), b'Arnold Rim')

        self.assertNoCalls()

    def test_w_readlines(self):
        file = self.get_w_file()

        self.assertEqual(file.readlines(), CONTENT.splitlines(keepends=True))

        self.assertNoCalls()

    def test_w_readlines_hint(self):
        file = self.get_w_file()

        self.assertEqual(file.readlines(40), CONTENT.splitlines(keepends=True)[:2])

        self.assertNoCalls()

    def test_w_seek(self):
        file = self.get_w_file()

        file.seek(10)

        self.assertEqual(file.tell(), 10)
        self.assertNoCalls()

    def test_w_seekable(self):
        file = self.get_w_file()

        self.assertTrue(file.seekable())

        self.assertNoCalls()

    def test_w_tell(self):
        file = self.get_w_file()

        self.assertEqual(file.tell(), 0)

        self.assertNoCalls()

    def test_w_truncate(self):
        file = self.get_w_file()

        self.assertRaises(UnsupportedOperation, file.truncate)

        self.assertNoCalls()

    def test_w_writable(self):
        # Test file is not writable in write mode.
        file = self.get_w_file(None)

        self.assertTrue(file.writable())

    def test_w_writelines(self):
        # Test writelines work in write mode.
        file = self.get_w_file(None)

        file.writelines(CONTENT.splitlines(keepends=True))

        file.seek(0)
        self.assertEqual(file.read(), CONTENT)
        self.assertNoCalls()

    def test_w_del(self):
        file = self.get_w_file()

        del file

        self.assertCreateCalled()

    def test_w_del_empty(self):
        file = self.get_w_file(None)

        del file

        self.assertNoCalls()

    def test_w_iter(self):
        file = self.get_w_file()

        iterator = iter(file)

        self.assertEqual(tuple(iterator), tuple(CONTENT.splitlines(keepends=True)))
        self.assertNoCalls()

    def test_w_next(self):
        file = self.get_w_file()

        self.assertEqual(next(file), b'Arnold Rimmer, Technician, 2nd Class.\n')

        self.assertNoCalls()

    # Binary I/O API - BufferedIOBase https://docs.python.org/3/library/io.html#io.BufferedIOBase
    def test_w_detach(self):
        file = self.get_w_file()

        self.assertRaises(UnsupportedOperation, file.detach)

        self.assertNoCalls()

    def test_w_read(self):
        file = self.get_w_file()

        self.assertEqual(file.read(), CONTENT)

        self.assertNoCalls()

    def test_w_read_size(self):
        file = self.get_w_file()

        self.assertEqual(file.read(size=10), b'Arnold Rim')

        self.assertNoCalls()

    def test_w_read1(self):
        file = self.get_w_file()

        self.assertRaises(UnsupportedOperation, file.read1)

        self.assertNoCalls()

    def test_w_readinto(self):
        file = self.get_w_file()

        array = bytearray(10)
        self.assertEqual(file.readinto(array), 10)

        self.assertEqual(array, CONTENT[:10])
        self.assertNoCalls()

    def test_w_readinto1(self):
        file = self.get_w_file()

        self.assertRaises(UnsupportedOperation, file.readinto1, bytearray(10))

        self.assertNoCalls()

    def test_w_write(self):
        # Test write works in write mode.
        file = self.get_w_file(None)

        self.assertEqual(file.write(CONTENT), len(CONTENT))

        file.seek(0)
        self.assertEqual(file.read(), CONTENT)
        self.assertNoCalls()

    # OTHER TESTS ######################################################################################################

    def test_init_no_args(self):
        with self.assertRaisesRegex(TypeError, "One of uid and name must be provided."):
            File(self.client)  # type: ignore[call-overload]

    def test_init_uid_name(self):
        with self.assertRaisesRegex(TypeError, "uid and name can't be both provided."):
            File(self.client, uid=A_UID, name=NAME)  # type: ignore[call-overload]

    def test_init_uid_mimetype(self):
        with self.assertRaisesRegex(TypeError, "uid and mimetype can't be both provided."):
            File(self.client, uid=A_UID, mimetype=MIMETYPE)  # type: ignore[call-overload]


class StorageTest(TestCase):
    def setUp(self):
        self.client = TestFileClient(sentinel.netloc)

    def _test_open(self, *args: Any, **kwargs: Any) -> None:
        storage = Storage(self.client)
        reply = StatReply()
        reply.data.name = NAME
        self.client.mock.return_value = reply

        file = storage.open(*args, **kwargs)

        self.assertEqual(file.name, NAME)
        request = StatRequest()
        request.uid.value = A_UID
        self.assertEqual(self.client.mock.mock_calls,
                         [call(request, method='/Fred.Fileman.Api.File/stat', timeout=None)])

    def test_open_uid_arg(self):
        self._test_open(A_UID)

    def test_open_uid_kwarg(self):
        self._test_open(uid=A_UID)

    def test_open_uuid_arg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_open(A_UUID)

    def test_open_uuid_kwarg(self):
        with self.assertWarnsRegex(DeprecationWarning, "Argument uuid is deprecated in favor of uid."):
            self._test_open(uuid=A_UUID)

    def test_create(self):
        storage = Storage(self.client)

        file = storage.create(name=NAME)

        self.assertEqual(file.name, NAME)
        self.assertEqual(self.client.mock.mock_calls, [])

    def test_create_mimetype(self):
        storage = Storage(self.client)

        file = storage.create(name=NAME, mimetype=MIMETYPE)

        self.assertEqual(file.name, NAME)
        self.assertEqual(file.mimetype, MIMETYPE)
        self.assertEqual(self.client.mock.mock_calls, [])
