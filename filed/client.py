"""File gRPC client."""
import warnings
from typing import Any, Dict, Iterable, Iterator, Optional, Union, cast, overload
from uuid import UUID

import grpc
from fred_api.fileman import service_fileman_grpc_pb2_grpc
from fred_api.fileman.service_fileman_grpc_pb2 import CreateRequest, ReadRequest, StatRequest
from frgal import GrpcClient, GrpcDecoder


class _ForwardGrpcDecoder(GrpcDecoder):
    """Forward compatible GrpcDecoder."""

    decode_unset_messages = False
    decode_empty_string_none = False


class FileClient(GrpcClient):
    """File gRPC client."""

    decoder_cls = _ForwardGrpcDecoder
    grpc_modules = [service_fileman_grpc_pb2_grpc]
    grpc_service = 'File'

    @overload
    def stat(self, uid: str) -> Dict[str, Any]:
        ...

    @overload
    def stat(self, uuid: UUID) -> Dict[str, Any]:
        ...

    def stat(self, uid: Optional[Union[str, UUID]] = None, *, uuid: Optional[UUID] = None,  # type: ignore[misc]
             ) -> Dict[str, Any]:
        """Get file metadata.

        Arguments:
            uid: A file UID.
            uuid: A file UUID. Deprecated in favor of `uid`.
        """
        if isinstance(uid, UUID):
            uid, uuid = None, uid
        request = StatRequest()
        if uuid:
            warnings.warn("Argument uuid is deprecated in favor of uid.", DeprecationWarning, stacklevel=2)
            request.uid.value = str(uuid)
        else:
            request.uid.value = uid

        try:
            return cast(Dict[str, Any], self.call(self.grpc_service, 'stat', request))
        except grpc.RpcError as error:
            if error.code() == grpc.StatusCode.NOT_FOUND:
                raise FileNotFoundError('File {} not found.'.format(uid or uuid)) from error
            raise ConnectionAbortedError('Connection failed: {}'.format(error)) from error

    @overload
    def read(self, uid: str, size: int = 0) -> Iterator[bytes]:
        ...

    @overload
    def read(self, uuid: UUID, size: int = 0) -> Iterator[bytes]:
        ...

    def read(self, uid: Optional[Union[str, UUID]] = None, size: int = 0,  # type: ignore[misc]
             *, uuid: Optional[UUID] = None) -> Iterator[bytes]:
        """Get file content.

        Arguments:
            uid: A file UID.
            size: A maximum size of a chunk.
            uuid: A file UUID. Deprecated in favor of `uid`.

        Raises:
            FileNotFoundError: If the file is not found.
            ConnectionAbortedError: Another error in connection occured.
        """
        if isinstance(uid, UUID):
            uid, uuid = None, uid
        request = ReadRequest()
        if uuid:
            warnings.warn("Argument uuid is deprecated in favor of uid.", DeprecationWarning, stacklevel=2)
            request.uid.value = str(uuid)
        else:
            request.uid.value = uid
        request.size = size
        try:
            yield from self.call_stream(self.grpc_service, 'read', request)
        except grpc.RpcError as error:
            if error.code() == grpc.StatusCode.NOT_FOUND:
                raise FileNotFoundError('File {} not found.'.format(uid or uuid)) from error
            raise ConnectionAbortedError('Connection failed: {}'.format(error)) from error

    def _make_create_requests(self, name: str, data: Iterable[bytes], mimetype: Optional[str] = None,
                              ) -> Iterator[CreateRequest]:
        """Return an iterator of create requests."""
        # Turn data into an iterator.
        for i, chunk in enumerate(data):
            request = CreateRequest()
            request.data = chunk
            if i == 0:
                request.name = name
                if mimetype:
                    request.mimetype = mimetype
            yield request

    def create(self, name: str, data: Iterable[bytes], *, mimetype: Optional[str] = None) -> str:
        """Create a new file.

        Arguments:
            name: A file name.
            data: A file content.
            mimetype: A file MIME type.

        Raises:
            ConnectionAbortedError: An error in connection occured.
        """
        try:
            result = self.call(self.grpc_service, 'create', self._make_create_requests(name, data, mimetype))
        except grpc.RpcError as error:
            raise ConnectionAbortedError('Connection failed: {}'.format(error)) from error
        return cast(str, result)
