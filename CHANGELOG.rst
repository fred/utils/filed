ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

0.4.0 (2024-09-25)
------------------

* Replace ``frgal`` with ``fred-frgal``.
* Update project setup.

0.3.1 (2024-01-31)
------------------

* Fix FileClient.read exception handling (#11).

0.3.0 (2023-11-14)
------------------

* Add support for python 3.11 and 3.12.
* Drop support for python 3.7.
* Use API 1.2.0 (#8).
* Update frgal (#10).
* Support UID in FileClient (#7).

  .. warning::
     ``FileClient.create`` now returns ``str`` instead of ``UUID``.

* Support UID in File and Storage (#7).
* Fix annotations.
* Update project setup.

0.2.1 (2022-02-02)
------------------

* Restrict dependency versions.

0.2.0 (2022-02-01)
------------------

* Add support for python 3.9 and 3.10.
* Drop support for python 3.5 and 3.6.
* Add ``FileClient.create`` (#1).
* Add write mode to ``File`` (#2).
* Add ``Storage.create`` (#2).
* Actually transport file content in chunks (#3).
* Update static checks & project setup.
* Update mypy settings & improve annotations.

0.1.0 (2020-06-16)
------------------

Initial version.

* Add read-only access to files with high level I/O interface.
